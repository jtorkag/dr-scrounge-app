﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;


namespace Scrounge
{
    public partial class Form1 : Form
    {
        StreamReader sr;
        StreamWriter sw;

        csvInterface csvInterface = new csvInterface();
        RollDetermination rollDetermination = new RollDetermination();

        Random rnd = new Random();

        public Form1()
        {
            InitializeComponent();
        }

        // array's holding scrounge tables

        public static string[,] scroungeContainer;

        public static int currentShift;

        private void Form1_Load(object sender, EventArgs e)
        {

            sr = new StreamReader("Shift Number.txt");
            currentShift = Convert.ToInt16(sr.ReadLine());
            sr.Close();

            txtCurrentShift.Text = Convert.ToString(currentShift);

            // create files if missing start 
            for (int x = 1; x < 5; x++)
            {
                string patha = Directory.GetCurrentDirectory() + "\\Scrounge Shift " + x;
                Directory.CreateDirectory(patha);
                if (File.Exists(Directory.GetCurrentDirectory() + "\\Scrounge Shift " + x + "\\Scrounge Master.csv"))
                {
                    continue;
                }
                else
                {
                    try
                    {
                        File.Copy("Scrounge Master.csv", Directory.GetCurrentDirectory() + "\\Scrounge Shift " + x + "\\Scrounge.csv");
                    }
                    catch
                    {
                    }
                }
            }
            string path = Directory.GetCurrentDirectory() + "\\Scrounge Shift " + currentShift;
            //populate scrounge container from file

            scroungeContainer = csvInterface.CreateArray(currentShift);
            // create files if missing end              
        }

        // prevent illegal user entry
        private void CmbScrapLevelFive_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        // prevent illegal user entry
        private void CmbHerbLevelFive_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void BtnReset_Click(object sender, EventArgs e)
        {
            if (chbResetCheck.Checked == true)
            {
                //reset Scrap scrounge Counts
                cmbScrapAmount.Text = "0";

                //reset Herb Scrounge Counts
                cmbHerbAmount.Text = "0";

                // Reset Scrap Results
                txtScrapBasicAmount.Text = "0";
                txtScrapUncommonAmount.Text = "0";
                txtScrapRareAmount.Text = "0";
                txtNamedScrap.Text = "";

                //Reset Herb Results
                txtHerbBasicAmount.Text = "0";
                txtHerbUncommonAmount.Text = "0";
                txtHerbRareAmount.Text = "0";
                txtNamedHerb.Text = "";

                //Reset Checks
                ChbReadyCheck.Checked = false;
                chbResetCheck.Checked = false;

                //Enable Scrounge Button
                btnScrounge.Enabled = true;

            }
            else
            {
                MessageBox.Show("Please make sure you are done and check the apppropriate box.");
            }
        }

        private void BtnScrounge_Click(object sender, EventArgs e)
        {
            if (ChbReadyCheck.Checked == true)
            {
                ChbReadyCheck.Checked = false;

                btnScrounge.Enabled = false;

                // sanatize input to not be null
                if (cmbScrapAmount.Text == "")
                {
                    cmbScrapAmount.Text = "0";
                }

                if (cmbHerbAmount.Text == "")
                {
                    cmbHerbAmount.Text = "0";
                }

                // roll for Scrap
                //only run if there's any cards requested to draw
                if (Convert.ToInt32(cmbScrapAmount.Text) > 0)
                {
                    rollDetermination.GetResults(scroungeContainer, 0, Convert.ToInt32(cmbScrapAmount.Text));
                }

                // roll for Herb
                //only run if there's any cards requested to draw
                if (Convert.ToInt32(cmbHerbAmount.Text) > 0)
                {
                    rollDetermination.GetResults(scroungeContainer, 3, Convert.ToInt32(cmbHerbAmount.Text));
                }
            }

            else
            {
                MessageBox.Show("Please make sure you are done and check the apppropriate box.");
            }
        }

        private void BtnShiftup_Click_1(object sender, EventArgs e)
        {
            if (chbShiftCheck.Checked == true)
            {

                string cListShift = "Shift Number.txt";
                string tListShift = "Shift Number Temp.txt";

                if (File.Exists(tListShift))
                {
                }
                else
                {
                    File.Create(tListShift).Close();
                }

                sr = new StreamReader(cListShift);
                sw = new StreamWriter(tListShift);

                string temp = sr.ReadLine();
                currentShift = Convert.ToInt16(temp);
                currentShift++;

                if (currentShift > 4)
                {
                    MessageBox.Show("You cant go past shift 4!");
                    currentShift--;
                }

                sw.WriteLine(Convert.ToString(currentShift));

                sr.Close();
                sw.Close();

                File.Delete(cListShift);
                System.IO.File.Move(tListShift, cListShift);

                txtCurrentShift.Text = Convert.ToString(currentShift);

                scroungeContainer = csvInterface.CreateArray(currentShift);
            }
            else
            {
                MessageBox.Show("Please Make sure you want to change shifts and check the appropirate box. ");
            }


            chbShiftCheck.Checked = false;
            ChbReadyCheck.Checked = false;
            chbResetCheck.Checked = false;
            btnScrounge.Enabled = false;

        }

        private void BtnShiftBack_Click_1(object sender, EventArgs e)
        {
            if (chbShiftCheck.Checked == true)
            {

                string cListShift = "Shift Number.txt";
                string tListShift = "Shift Number Temp.txt";

                if (!File.Exists(tListShift))
                {
                    File.Create(tListShift).Close();
                }


                sr = new StreamReader(cListShift);
                sw = new StreamWriter(tListShift);

                string temp = sr.ReadLine();
                currentShift = Convert.ToInt16(temp);
                currentShift--;

                if (currentShift < 1)
                {
                    MessageBox.Show("You cant go below shift one!");
                    currentShift++;
                }

                sw.WriteLine(Convert.ToString(currentShift));

                sr.Close();
                sw.Close();

                File.Delete(cListShift);
                System.IO.File.Move(tListShift, cListShift);


                txtCurrentShift.Text = Convert.ToString(currentShift);

                scroungeContainer = csvInterface.CreateArray(currentShift);
            }
            else
            {
                MessageBox.Show("Please Make sure you want to change shifts and check the appropirate box. ");
            }


            chbShiftCheck.Checked = false;
            ChbReadyCheck.Checked = false;
            chbResetCheck.Checked = false;
            btnScrounge.Enabled = false;
        }
    }
}










