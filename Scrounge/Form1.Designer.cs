﻿namespace Scrounge
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnScrounge = new System.Windows.Forms.Button();
            this.ChbReadyCheck = new System.Windows.Forms.CheckBox();
            this.cmbScrapAmount = new System.Windows.Forms.ComboBox();
            this.cmbHerbAmount = new System.Windows.Forms.ComboBox();
            this.btnReset = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblScrapResult = new System.Windows.Forms.Label();
            this.lblHerbResult = new System.Windows.Forms.Label();
            Form1.txtScrapBasicAmount = new System.Windows.Forms.TextBox();
            this.lblScrapLevelResult = new System.Windows.Forms.Label();
            Form1.txtScrapUncommonAmount = new System.Windows.Forms.TextBox();
            Form1.txtScrapRareAmount = new System.Windows.Forms.TextBox();
            Form1.txtHerbBasicAmount = new System.Windows.Forms.TextBox();
            Form1.txtHerbUncommonAmount = new System.Windows.Forms.TextBox();
            Form1.txtHerbRareAmount = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            Form1.txtNamedScrap = new System.Windows.Forms.TextBox();
            Form1.txtNamedHerb = new System.Windows.Forms.TextBox();
            this.chbResetCheck = new System.Windows.Forms.CheckBox();
            this.lblScroungeTypeScrap = new System.Windows.Forms.Label();
            this.lblScroungeTypeHerb = new System.Windows.Forms.Label();
            this.chbShiftCheck = new System.Windows.Forms.CheckBox();
            this.txtCurrentShift = new System.Windows.Forms.TextBox();
            this.lblShiftCheck = new System.Windows.Forms.Label();
            this.btnShiftBack = new System.Windows.Forms.Button();
            this.btnShiftup = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnScrounge
            // 
            this.btnScrounge.Location = new System.Drawing.Point(220, 80);
            this.btnScrounge.Name = "btnScrounge";
            this.btnScrounge.Size = new System.Drawing.Size(75, 23);
            this.btnScrounge.TabIndex = 99;
            this.btnScrounge.TabStop = false;
            this.btnScrounge.Text = "Scrounge";
            this.btnScrounge.UseVisualStyleBackColor = true;
            this.btnScrounge.Click += new System.EventHandler(this.BtnScrounge_Click);
            // 
            // ChbReadyCheck
            // 
            this.ChbReadyCheck.AutoSize = true;
            this.ChbReadyCheck.Location = new System.Drawing.Point(220, 109);
            this.ChbReadyCheck.Name = "ChbReadyCheck";
            this.ChbReadyCheck.Size = new System.Drawing.Size(129, 17);
            this.ChbReadyCheck.TabIndex = 98;
            this.ChbReadyCheck.TabStop = false;
            this.ChbReadyCheck.Text = "Is everything Correct?";
            this.ChbReadyCheck.UseVisualStyleBackColor = true;
            // 
            // cmbScrapAmount
            // 
            this.cmbScrapAmount.FormattingEnabled = true;
            this.cmbScrapAmount.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8"});
            this.cmbScrapAmount.Location = new System.Drawing.Point(47, 74);
            this.cmbScrapAmount.Name = "cmbScrapAmount";
            this.cmbScrapAmount.Size = new System.Drawing.Size(43, 21);
            this.cmbScrapAmount.TabIndex = 5;
            this.cmbScrapAmount.Text = "0";
            this.cmbScrapAmount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CmbScrapLevelFive_KeyPress);
            // 
            // cmbHerbAmount
            // 
            this.cmbHerbAmount.FormattingEnabled = true;
            this.cmbHerbAmount.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8"});
            this.cmbHerbAmount.Location = new System.Drawing.Point(131, 74);
            this.cmbHerbAmount.Name = "cmbHerbAmount";
            this.cmbHerbAmount.Size = new System.Drawing.Size(43, 21);
            this.cmbHerbAmount.TabIndex = 10;
            this.cmbHerbAmount.Text = "0";
            this.cmbHerbAmount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CmbHerbLevelFive_KeyPress);
            // 
            // btnReset
            // 
            this.btnReset.Location = new System.Drawing.Point(219, 26);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(75, 23);
            this.btnReset.TabIndex = 11;
            this.btnReset.TabStop = false;
            this.btnReset.Text = "Reset";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.BtnReset_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(40, 124);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(134, 39);
            this.label1.TabIndex = 100;
            this.label1.Text = "Results";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(32, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(165, 39);
            this.label2.TabIndex = 101;
            this.label2.Text = "Scrounge";
            // 
            // lblScrapResult
            // 
            this.lblScrapResult.AutoSize = true;
            this.lblScrapResult.Location = new System.Drawing.Point(68, 169);
            this.lblScrapResult.Name = "lblScrapResult";
            this.lblScrapResult.Size = new System.Drawing.Size(35, 13);
            this.lblScrapResult.TabIndex = 102;
            this.lblScrapResult.Text = "Scrap";
            // 
            // lblHerbResult
            // 
            this.lblHerbResult.AutoSize = true;
            this.lblHerbResult.Location = new System.Drawing.Point(148, 169);
            this.lblHerbResult.Name = "lblHerbResult";
            this.lblHerbResult.Size = new System.Drawing.Size(30, 13);
            this.lblHerbResult.TabIndex = 103;
            this.lblHerbResult.Text = "Herb";
            // 
            // txtScrapBasicAmount
            // 
            Form1.txtScrapBasicAmount.BackColor = System.Drawing.SystemColors.Window;
            Form1.txtScrapBasicAmount.Location = new System.Drawing.Point(68, 185);
            Form1.txtScrapBasicAmount.Name = "txtScrapBasicAmount";
            Form1.txtScrapBasicAmount.ReadOnly = true;
            Form1.txtScrapBasicAmount.Size = new System.Drawing.Size(33, 20);
            Form1.txtScrapBasicAmount.TabIndex = 104;
            Form1.txtScrapBasicAmount.TabStop = false;
            Form1.txtScrapBasicAmount.Text = "0";
            // 
            // lblScrapLevelResult
            // 
            this.lblScrapLevelResult.AutoSize = true;
            this.lblScrapLevelResult.Location = new System.Drawing.Point(29, 188);
            this.lblScrapLevelResult.Name = "lblScrapLevelResult";
            this.lblScrapLevelResult.Size = new System.Drawing.Size(33, 65);
            this.lblScrapLevelResult.TabIndex = 105;
            this.lblScrapLevelResult.Text = "Basic\r\n\r\nUnC\r\n\r\nRare\r\n";
            // 
            // txtScrapUncommonAmount
            // 
            Form1.txtScrapUncommonAmount.BackColor = System.Drawing.SystemColors.Window;
            Form1.txtScrapUncommonAmount.Location = new System.Drawing.Point(68, 211);
            Form1.txtScrapUncommonAmount.Name = "txtScrapUncommonAmount";
            Form1.txtScrapUncommonAmount.ReadOnly = true;
            Form1.txtScrapUncommonAmount.Size = new System.Drawing.Size(33, 20);
            Form1.txtScrapUncommonAmount.TabIndex = 106;
            Form1.txtScrapUncommonAmount.TabStop = false;
            Form1.txtScrapUncommonAmount.Text = "0";
            // 
            // txtScrapRareAmount
            // 
            Form1.txtScrapRareAmount.BackColor = System.Drawing.SystemColors.Window;
            Form1.txtScrapRareAmount.Location = new System.Drawing.Point(68, 237);
            Form1.txtScrapRareAmount.Name = "txtScrapRareAmount";
            Form1.txtScrapRareAmount.ReadOnly = true;
            Form1.txtScrapRareAmount.Size = new System.Drawing.Size(33, 20);
            Form1.txtScrapRareAmount.TabIndex = 107;
            Form1.txtScrapRareAmount.TabStop = false;
            Form1.txtScrapRareAmount.Text = "0";
            // 
            // txtHerbBasicAmount
            // 
            Form1.txtHerbBasicAmount.BackColor = System.Drawing.SystemColors.Window;
            Form1.txtHerbBasicAmount.Location = new System.Drawing.Point(145, 185);
            Form1.txtHerbBasicAmount.Name = "txtHerbBasicAmount";
            Form1.txtHerbBasicAmount.ReadOnly = true;
            Form1.txtHerbBasicAmount.Size = new System.Drawing.Size(33, 20);
            Form1.txtHerbBasicAmount.TabIndex = 108;
            Form1.txtHerbBasicAmount.TabStop = false;
            Form1.txtHerbBasicAmount.Text = "0";
            // 
            // txtHerbUncommonAmount
            // 
            Form1.txtHerbUncommonAmount.BackColor = System.Drawing.SystemColors.Window;
            Form1.txtHerbUncommonAmount.Location = new System.Drawing.Point(145, 211);
            Form1.txtHerbUncommonAmount.Name = "txtHerbUncommonAmount";
            Form1.txtHerbUncommonAmount.ReadOnly = true;
            Form1.txtHerbUncommonAmount.Size = new System.Drawing.Size(33, 20);
            Form1.txtHerbUncommonAmount.TabIndex = 109;
            Form1.txtHerbUncommonAmount.TabStop = false;
            Form1.txtHerbUncommonAmount.Text = "0";
            // 
            // txtHerbRareAmount
            // 
            Form1.txtHerbRareAmount.BackColor = System.Drawing.SystemColors.Window;
            Form1.txtHerbRareAmount.Location = new System.Drawing.Point(145, 237);
            Form1.txtHerbRareAmount.Name = "txtHerbRareAmount";
            Form1.txtHerbRareAmount.ReadOnly = true;
            Form1.txtHerbRareAmount.Size = new System.Drawing.Size(33, 20);
            Form1.txtHerbRareAmount.TabIndex = 110;
            Form1.txtHerbRareAmount.TabStop = false;
            Form1.txtHerbRareAmount.Text = "0";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(106, 188);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 65);
            this.label3.TabIndex = 111;
            this.label3.Text = "Basic\r\n\r\nUnC\r\n\r\nRare\r\n";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(47, 291);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 13);
            this.label4.TabIndex = 112;
            this.label4.Text = "Named Scrap";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(228, 291);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(67, 13);
            this.label5.TabIndex = 113;
            this.label5.Text = "Named Herb";
            // 
            // txtNamedScrap
            // 
            Form1.txtNamedScrap.BackColor = System.Drawing.SystemColors.Window;
            Form1.txtNamedScrap.Location = new System.Drawing.Point(12, 307);
            Form1.txtNamedScrap.Multiline = true;
            Form1.txtNamedScrap.Name = "txtNamedScrap";
            Form1.txtNamedScrap.ReadOnly = true;
            Form1.txtNamedScrap.Size = new System.Drawing.Size(162, 133);
            Form1.txtNamedScrap.TabIndex = 114;
            Form1.txtNamedScrap.TabStop = false;
            // 
            // txtNamedHerb
            // 
            Form1.txtNamedHerb.BackColor = System.Drawing.SystemColors.Window;
            Form1.txtNamedHerb.Location = new System.Drawing.Point(187, 307);
            Form1.txtNamedHerb.Multiline = true;
            Form1.txtNamedHerb.Name = "txtNamedHerb";
            Form1.txtNamedHerb.ReadOnly = true;
            Form1.txtNamedHerb.Size = new System.Drawing.Size(162, 133);
            Form1.txtNamedHerb.TabIndex = 115;
            Form1.txtNamedHerb.TabStop = false;
            // 
            // chbResetCheck
            // 
            this.chbResetCheck.AutoSize = true;
            this.chbResetCheck.Location = new System.Drawing.Point(219, 57);
            this.chbResetCheck.Name = "chbResetCheck";
            this.chbResetCheck.Size = new System.Drawing.Size(95, 17);
            this.chbResetCheck.TabIndex = 116;
            this.chbResetCheck.TabStop = false;
            this.chbResetCheck.Text = "Are you done?";
            this.chbResetCheck.UseVisualStyleBackColor = true;
            // 
            // lblScroungeTypeScrap
            // 
            this.lblScroungeTypeScrap.AutoSize = true;
            this.lblScroungeTypeScrap.Location = new System.Drawing.Point(47, 58);
            this.lblScroungeTypeScrap.Name = "lblScroungeTypeScrap";
            this.lblScroungeTypeScrap.Size = new System.Drawing.Size(35, 13);
            this.lblScroungeTypeScrap.TabIndex = 0;
            this.lblScroungeTypeScrap.Text = "Scrap";
            // 
            // lblScroungeTypeHerb
            // 
            this.lblScroungeTypeHerb.AutoSize = true;
            this.lblScroungeTypeHerb.Location = new System.Drawing.Point(138, 58);
            this.lblScroungeTypeHerb.Name = "lblScroungeTypeHerb";
            this.lblScroungeTypeHerb.Size = new System.Drawing.Size(30, 13);
            this.lblScroungeTypeHerb.TabIndex = 1;
            this.lblScroungeTypeHerb.Text = "Herb";
            // 
            // chbShiftCheck
            // 
            this.chbShiftCheck.AutoSize = true;
            this.chbShiftCheck.Location = new System.Drawing.Point(209, 225);
            this.chbShiftCheck.Name = "chbShiftCheck";
            this.chbShiftCheck.Size = new System.Drawing.Size(87, 17);
            this.chbShiftCheck.TabIndex = 126;
            this.chbShiftCheck.TabStop = false;
            this.chbShiftCheck.Text = "Change Shift";
            this.chbShiftCheck.UseVisualStyleBackColor = true;
            // 
            // txtCurrentShift
            // 
            this.txtCurrentShift.Location = new System.Drawing.Point(219, 185);
            this.txtCurrentShift.Name = "txtCurrentShift";
            this.txtCurrentShift.Size = new System.Drawing.Size(25, 20);
            this.txtCurrentShift.TabIndex = 125;
            this.txtCurrentShift.TabStop = false;
            // 
            // lblShiftCheck
            // 
            this.lblShiftCheck.AutoSize = true;
            this.lblShiftCheck.Location = new System.Drawing.Point(206, 169);
            this.lblShiftCheck.Name = "lblShiftCheck";
            this.lblShiftCheck.Size = new System.Drawing.Size(65, 13);
            this.lblShiftCheck.TabIndex = 124;
            this.lblShiftCheck.Text = "Current Shift";
            // 
            // btnShiftBack
            // 
            this.btnShiftBack.Location = new System.Drawing.Point(277, 196);
            this.btnShiftBack.Name = "btnShiftBack";
            this.btnShiftBack.Size = new System.Drawing.Size(75, 23);
            this.btnShiftBack.TabIndex = 123;
            this.btnShiftBack.TabStop = false;
            this.btnShiftBack.Text = "Last Shift";
            this.btnShiftBack.UseVisualStyleBackColor = true;
            this.btnShiftBack.Click += new System.EventHandler(this.BtnShiftBack_Click_1);
            // 
            // btnShiftup
            // 
            this.btnShiftup.Location = new System.Drawing.Point(277, 169);
            this.btnShiftup.Name = "btnShiftup";
            this.btnShiftup.Size = new System.Drawing.Size(75, 23);
            this.btnShiftup.TabIndex = 122;
            this.btnShiftup.TabStop = false;
            this.btnShiftup.Text = "Next Shift";
            this.btnShiftup.UseVisualStyleBackColor = true;
            this.btnShiftup.Click += new System.EventHandler(this.BtnShiftup_Click_1);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(364, 448);
            this.Controls.Add(this.chbShiftCheck);
            this.Controls.Add(this.txtCurrentShift);
            this.Controls.Add(this.lblShiftCheck);
            this.Controls.Add(this.btnShiftBack);
            this.Controls.Add(this.btnShiftup);
            this.Controls.Add(this.chbResetCheck);
            this.Controls.Add(Form1.txtNamedHerb);
            this.Controls.Add(Form1.txtNamedScrap);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(Form1.txtHerbRareAmount);
            this.Controls.Add(Form1.txtHerbUncommonAmount);
            this.Controls.Add(Form1.txtHerbBasicAmount);
            this.Controls.Add(Form1.txtScrapRareAmount);
            this.Controls.Add(Form1.txtScrapUncommonAmount);
            this.Controls.Add(this.lblScrapLevelResult);
            this.Controls.Add(Form1.txtScrapBasicAmount);
            this.Controls.Add(this.lblHerbResult);
            this.Controls.Add(this.lblScrapResult);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.cmbHerbAmount);
            this.Controls.Add(this.cmbScrapAmount);
            this.Controls.Add(this.ChbReadyCheck);
            this.Controls.Add(this.btnScrounge);
            this.Controls.Add(this.lblScroungeTypeHerb);
            this.Controls.Add(this.lblScroungeTypeScrap);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Scounge App";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnScrounge;
        private System.Windows.Forms.CheckBox ChbReadyCheck;
        private System.Windows.Forms.ComboBox cmbScrapAmount;
        private System.Windows.Forms.ComboBox cmbHerbAmount;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblScrapResult;
        private System.Windows.Forms.Label lblHerbResult;
        public static System.Windows.Forms.TextBox txtScrapBasicAmount;
        private System.Windows.Forms.Label lblScrapLevelResult;
        public static System.Windows.Forms.TextBox txtScrapUncommonAmount;
        public static System.Windows.Forms.TextBox txtScrapRareAmount;
        public static System.Windows.Forms.TextBox txtHerbBasicAmount;
        public static System.Windows.Forms.TextBox txtHerbUncommonAmount;
        public static System.Windows.Forms.TextBox txtHerbRareAmount;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        public static System.Windows.Forms.TextBox txtNamedScrap;
        public static System.Windows.Forms.TextBox txtNamedHerb;
        private System.Windows.Forms.CheckBox chbResetCheck;
        private System.Windows.Forms.Label lblScroungeTypeScrap;
        private System.Windows.Forms.Label lblScroungeTypeHerb;
        private System.Windows.Forms.CheckBox chbShiftCheck;
        private System.Windows.Forms.TextBox txtCurrentShift;
        private System.Windows.Forms.Label lblShiftCheck;
        private System.Windows.Forms.Button btnShiftBack;
        private System.Windows.Forms.Button btnShiftup;
    }
}

