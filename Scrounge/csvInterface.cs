﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.IO;

namespace Scrounge
{
    class csvInterface
    {
        StreamWriter sw;
        StreamReader sr;

        public string[,] CreateArray(int shift)
        {
            string cList = Directory.GetCurrentDirectory() + "\\Scrounge Shift " + shift + "\\Scrounge.csv";

            int lengthScrap;
            int lengthHerb;
            string[] cursor;

            sr = new StreamReader(cList);
             
            // iterate to named scrap row
            for (int z = 0; z < 22; z++)
            {
                sr.ReadLine();
            }
           
            // check named scrap amount
            cursor = sr.ReadLine().Split(',');
            lengthScrap = Convert.ToInt16(cursor[1]);

            // check named herb amount
            cursor = sr.ReadLine().Split(',');
            lengthHerb = Convert.ToInt16(cursor[1]);

            sr.Close();

            // create array
            sr = new StreamReader(cList);
            string line = sr.ReadLine();
            string[] lineArray = new string[3];

            int length = 0;

            if (lengthScrap > length)
            {
                length = lengthScrap;
            }
            if (lengthHerb > length)
            {
                length = lengthHerb;
            }

            string[,] currentList = new string[25, length + 2];
            for (int y = 0; y < 20; y++)
            {
                for (int x = 0; x < 2; x++)
                {

                    if (x == 0)
                    {

                        if (y < 6)
                        {
                            currentList[y, x] = "1";
                        }
                        else if (y > 5 && y < 12)
                        {
                            currentList[y, x] = "2";
                        }
                        else if (y > 11 && y < 18)
                        {
                            currentList[y, x] = "3";
                        }
                    }
                    else if (x == 1)
                    {
                        line = sr.ReadLine();
                        lineArray = line.Split(',');
                        if (y < 6)
                        {
                            currentList[y, x] = lineArray[2];
                        }
                        else if (y > 6 && y < 13)
                        {
                            currentList[y - 1, x] = lineArray[2];
                        }
                        else if (y > 13)
                        {
                            currentList[y - 2, x] = lineArray[2];
                        }

                    }
                }

            }

            sr.ReadLine();

            string[] namedItemsArray = new string[length + 2];

            // Create named scrap line
            string readNamedScrap = sr.ReadLine();
            namedItemsArray = readNamedScrap.Split(',');
            for (int x = 0; x < lengthScrap + 2; x++)
            {
                currentList[18, x] = namedItemsArray[x];
            }

            // create named herb line
            string readNamedHerb = sr.ReadLine();
            namedItemsArray = readNamedHerb.Split(',');
            for (int x = 0; x < lengthHerb + 2; x++)
            {
                currentList[19, x] = namedItemsArray[x];
            }
            sr.Close();
            return currentList;
        }

        public void WriteToList(int row, int shift)

        {
            // string cList = "Scrounge " + list + ".csv";
            string cList = Directory.GetCurrentDirectory() + "\\Scrounge Shift " + shift + "\\Scrounge.csv";
            string tList = Directory.GetCurrentDirectory() + "\\Scrounge Shift " + shift + "\\Scrounge Temp.csv";

            if (File.Exists(tList))
            {
            }
            else
            {
                File.Create(tList).Close();
            }

            sr = new StreamReader(cList);
            sw = new StreamWriter(tList);

            string rowString;

            string[] rowArray = new string[3];

            //int[,] currentList = new int[18, 2];
            for (int y = 0; y < 30; y++)
            {
                if (y != row)
                {
                    rowString = sr.ReadLine();
                    sw.WriteLine(rowString);
                }
                else if (y == row)
                {
                    rowString = sr.ReadLine();
                    rowArray = rowString.Split(',');

                    int math = Convert.ToInt16(rowArray[2]) - 1;

                    rowArray[2] = Convert.ToString(math);

                    rowString = String.Join(",", rowArray);
                    sw.WriteLine(rowString);
                }
                else
                {
                }
            }

            sr.Close();
            sw.Close();

            File.Delete(cList);
            System.IO.File.Move(tList, cList);
        }

        public void WriteToListNamed(int row, int shift, int position, int length)

        {
            // string cList = "Scrounge " + list + ".csv";
            string cList = Directory.GetCurrentDirectory() + "\\Scrounge Shift " + shift + "\\Scrounge.csv";
            string tList = Directory.GetCurrentDirectory() + "\\Scrounge Shift " + shift + "\\Scrounge Temp.csv";

            if (File.Exists(tList))
            {
            }
            else
            {
                File.Create(tList).Close();
            }

            sr = new StreamReader(cList);
            sw = new StreamWriter(tList);

            string line;

            string[] lineArray = new string[length];

            //int[,] currentList = new int[18, 2];
            for (int y = 0; y < 25; y++)
            {
                if (y != row)
                {
                    line = sr.ReadLine();
                    sw.WriteLine(line);
                }
                if (y == row)
                {

                    line = sr.ReadLine();
                    lineArray = line.Split(',');

                    lineArray[position] = "";

                    string[] updatedNamed = new string[lineArray.Length - 1];

                    bool removal = false;

                    for (int r = 0; r < lineArray.Length; r++)
                    {

                        if (lineArray[r] == "")
                        {

                            removal = true;
                            continue;
                        }
                        if (lineArray[r] != "")
                        {

                            if (removal == false)
                            {
                                updatedNamed[r] = lineArray[r];
                            }
                            if (removal == true)
                            {
                                updatedNamed[r - 1] = lineArray[r];
                            }
                        }


                    }

                    updatedNamed[1] = Convert.ToString(Convert.ToInt16(updatedNamed[1]) - 1);

                    line = String.Join(",", updatedNamed);
                    sw.WriteLine(line);
                }
            }
            sr.Close();
            sw.Close();

            File.Delete(cList);
            System.IO.File.Move(tList, cList);
        }
    }
}
