﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;
using System.Reflection;

namespace Scrounge
{
    class RollDetermination
    {
        int chancePool;
        int chancePool1;
        int chancePool2;
        int chancePool3;
        int chancePool4;
        int roll;
        bool breakCheck = false;
        Random rnd = new Random();
        csvInterface csvInterface = new csvInterface();

        public void GetResults(string[,] scroungeContainer, int type, int draws) // type determining scrap(0) or herb(3), draws being how many times.
        {
            for (int a = 0; a < draws; a++)
            {
                breakCheck = DetermineChancePool(scroungeContainer, type);
                if (breakCheck == true)
                {
                    break;
                }

                roll = rnd.Next(1, chancePool + 1);

                DetermineQuantity(roll, type);
            }
        }

        private bool DetermineChancePool(string[,] scroungeContainer, int type)
        {
            chancePool1 = Convert.ToInt16(scroungeContainer[0 + type * 3, 1]) + Convert.ToInt16(scroungeContainer[1 + type * 3, 1]) + Convert.ToInt16(scroungeContainer[2 + type * 3, 1]);
            chancePool2 = Convert.ToInt16(scroungeContainer[6 + type * 3, 1]) + Convert.ToInt16(scroungeContainer[7 + type * 3, 1]) + Convert.ToInt16(scroungeContainer[8 + type * 3, 1]);
            chancePool3 = Convert.ToInt16(scroungeContainer[12 + type * 3, 1]) + Convert.ToInt16(scroungeContainer[13 + type * 3, 1]) + Convert.ToInt16(scroungeContainer[14 + type * 3, 1]);
            chancePool4 = Convert.ToInt16(scroungeContainer[18 + type, 1]);
            chancePool = chancePool1 + chancePool2 + chancePool3 + chancePool4;

            if (chancePool < 1)
            {
                MessageBox.Show("Out of scrap cards");
                return true;
            }
            else
            {
                return false;
            }
        }

        // determine quanity or if named result
        private void DetermineQuantity(int roll, int type)
        {
            // quantity one
            if (roll <= chancePool - chancePool2 - chancePool3 - chancePool4)
            {
                DetermineRarity(roll, 1, type);
            }
            // quantity two
            else if (roll > chancePool1 && roll <= chancePool - chancePool3 - chancePool4)
            {
                DetermineRarity(roll, 2, type);
            }
            // quantity three
            else if (roll > chancePool1 + chancePool2 && roll <= chancePool - chancePool4)
            {
                DetermineRarity(roll, 3, type);
            }
            // roll for named
            else if (roll > chancePool1 + chancePool2 + chancePool3)
            {
                UpdateGUI();
            }
        }
        // if not named, determine rarity
        private void DetermineRarity(int roll, int quantity, int type)
        {
            // basic
            if (roll <= Convert.ToInt16(Form1.scroungeContainer[type + (quantity * 6) - 6, 1]))
            {
                UpdateGUI(0, quantity, type);
            }
            // uncommon
            else if (roll > Convert.ToInt16(Form1.scroungeContainer[type + (quantity * 6) - 6, 1]) && roll <= Convert.ToInt16(Form1.scroungeContainer[type + (quantity * 6) - 6, 1]) + Convert.ToInt16(Form1.scroungeContainer[type + (quantity * 6) - 6 + 1, 1]))
            {
                UpdateGUI(1, quantity, type);
            }
            //rare
            else if (roll > Convert.ToInt16(Form1.scroungeContainer[type + (quantity * 6) - 6, 1]) + Convert.ToInt16(Form1.scroungeContainer[type + (quantity * 6) - 6 + 1, 1]))
            {
                UpdateGUI(2, quantity, type);
            }

            }
        private void UpdateGUI(int rarity, int quantity,int type)
        {
            TextBox[] textField = new TextBox[] { Form1.txtScrapBasicAmount , Form1.txtScrapUncommonAmount, Form1.txtScrapRareAmount, Form1.txtHerbBasicAmount, Form1.txtHerbUncommonAmount, Form1.txtHerbRareAmount};

            textField[rarity + type].Text = Convert.ToString(Convert.ToInt16(textField[rarity + type].Text) + quantity);
            UpdateCSV(rarity, quantity, type);

        }

        private void UpdateCSV(int rarity, int quantity, int type)
        {
            Form1.scroungeContainer[type + (quantity * 6) - 6 + rarity, 1] = Convert.ToString(Convert.ToInt16(Form1.scroungeContainer[type + (quantity * 6) - 6 + rarity, 1]) - 1);
            csvInterface.WriteToList(type + (quantity * 6) - 6 + rarity + quantity, Form1.currentShift);

        }

        private void UpdateGUI()
        {             
            int namedRoll = rnd.Next(1, chancePool4 + 1);

            Form1.txtNamedScrap.Text = Form1.txtNamedScrap.Text + Form1.scroungeContainer[18, namedRoll + 1] + "\r\n";

            UpdateCSV(namedRoll);

        }

        private int CheckGreaterNamedType()
        {

            int checkGreater = 0;

            if (Convert.ToInt16(Form1.scroungeContainer[18, 1]) > Convert.ToInt16(Form1.scroungeContainer[19, 1]))
            {
                checkGreater = Convert.ToInt16(Form1.scroungeContainer[18, 1]);
            }
            if (Convert.ToInt16(Form1.scroungeContainer[19, 1]) > Convert.ToInt16(Form1.scroungeContainer[18, 1]))
            {
                checkGreater = Convert.ToInt16(Form1.scroungeContainer[19, 1]);
            }
            if (Convert.ToInt16(Form1.scroungeContainer[18, 1]) == Convert.ToInt16(Form1.scroungeContainer[19, 1]))
            {
                checkGreater = Convert.ToInt16(Form1.scroungeContainer[18, 1]);
            }

            return checkGreater;
        }

        private void UpdateCSV(int namedRoll)
        {
            csvInterface.WriteToListNamed(22, Form1.currentShift, namedRoll + 1, CheckGreaterNamedType() + 2);
        }
    }
}
